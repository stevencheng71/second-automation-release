# [1.1.0](https://gitlab.com/stevencheng71/second-automation-release/compare/v1.0.0...v1.1.0) (2024-2-24)


### Features

* add husky and commitlint ([2f78cf1](https://gitlab.com/stevencheng71/second-automation-release/commit/2f78cf1eae7a865fdd8d766939a41e93d6398e46))

# 1.0.0 (2024-02-24)


### Features
* add husky and commitlint ([a5092e3](https://gitlab.com/stevencheng71/second-automation-release/commit/a5092e33df6e2d83619c8fb66f6c1d527084c8df))
